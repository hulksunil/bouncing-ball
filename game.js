class Game {
  constructor() {
    this.targets_hit = 0;
    this.isPlaying = false;
    var label = $("#score").get(0);
    this.timer = new Timer(label);
    this.targetsHitLbl = $("#targetsHitLbl").get(0);
    this.targetsNeededToHit = 50;
  }
  incrementTargetsHit() {
    this.targets_hit++;
    this.targetsHitLbl.innerHTML = this.targets_hit;
    if (this.targets_hit == this.targetsNeededToHit) {
      this._compareHighscoreTime();
      this._stopGame();
    }
  }
  _compareHighscoreTime() {
    var highscore = $("#highscore").get(0);
    var timePieces = highscore.innerHTML.split(":");
    var highHour = parseInt(timePieces[0]);
    var highMin = parseInt(timePieces[1]);
    var highSecs = parseInt(timePieces[2]);

    var time = game.timer.getTime();
    var hour = time.hour;
    var min = time.min;
    var secs = time.secs;

    // If it wasn't set before (this isn't really correct but whatever)
    if (highHour === 0 && highMin === 0 && highSecs === 0) {
      highscore.innerHTML = $("#score").get(0).innerHTML;
    }
    // for a new highscore
    else if (hour < highHour || min < highMin || secs < highSecs) {
      highscore.innerHTML = $("#score").get(0).innerHTML;
    }
  }
  _stopGame() {
    // add the timer values to a table
    this.timer.stopTimer();
    this.targets_hit = 0;
    handlePlayStopGame();
  }
}

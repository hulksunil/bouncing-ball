var canvas;
var ctx;
var drawLineToTarget;
var backgroundIndex;
var game;

// Drawn objects
var ball;
var target;

/***
 *
 * Need to prompt to appear to let them know how many targets they need to hit.
 * Then it should probably just start a 3 second timer (with some racing starting sounds) and then start
 * timer.
 *
 *
 * Should also implement a decremential feature where if they hit the wall, 5 seconds is added to their time.
 * We should also capture how many times they hit a wall (or ground/cieling)
 *
 *
 * Also need a prompt for when they win
 *
 *
 */

/**
 * NEED TO CHANGE SCORE TO USE A TIMER INSTEAD AND THEY HAVE TO HIT LIKE 50. THE SCORE WILL BE THE TIME
 * IT TOOK TO HIT ALL 50. WE CAN ALSO HAVE LEVELS GOING UP TO 100 OR 150.
 */

function Ball() {
  (this.x = 30),
    (this.y = 375),
    (this.xVel = 10),
    (this.yVel = -15),
    (this.power = 1),
    (this.bounciness = $("#bouncinessRange")[0].value),
    (this.radius = (canvas.height * canvas.width) / 51000),
    (this.colors = [
      "rgb(255,0,0)", //* red
      "rgb(255,128,0)",
      "rgb(255,255,0)",
      "rgb(128,255,0)",
      "rgb(0,255,0)", //* green
      "rgb(0,255,128)",
      "rgb(0,255,255)",
      "rgb(0,128,255)",
      "rgb(0,0,255)", //* blue
      "rgb(128,0,255)",
      "rgb(255,0,255)",
      "rgb(255,0,128)",
    ]),
    (this.colorIndex = 0),
    (this.draw = function () {
      //ball
      ctx.beginPath();
      ctx.arc(this.x, this.y, this.radius, 0, Math.PI * 2, false);
      ctx.closePath();
      ctx.fillStyle = this.colors[this.colorIndex];
      ctx.fill();
    }),
    (this.changeColor = function () {
      this.colorIndex++;
      backgroundIndex++;
      if (this.colorIndex === this.colors.length) {
        this.colorIndex = 0;
      }
      canvas.style.backgroundColor = changeBackground();
    }),
    (this.checkTop = function () {
      // If the ball hits the top
      if (this.y - this.radius < 0) {
        this.y = 0 + this.radius;
        this.yVel = -this.yVel * this.bounciness;
      }
    }),
    (this.checkBottom = function () {
      // If the ball hits the bottom
      if (this.y + this.radius > canvas.height) {
        this.y = canvas.height - this.radius;
        this.yVel = -this.yVel * this.bounciness; // BOUNCINESS *SHOULDN'T BE HIGHER THAN 1*

        // To gradually slow down while the ball is hitting the ground
        this.xVel = this.xVel - this.xVel / 128;
      }
    }),
    (this.checkRight = function () {
      // If ball hits the right side
      if (this.x + this.radius >= canvas.width) {
        this.changeColor();
        this.x = canvas.width - this.radius;
        this.xVel = -this.xVel + this.xVel / 8;
      }
    }),
    (this.checkLeft = function () {
      // If ball hits the left side
      if (this.x - this.radius <= 0) {
        this.x = 0 + this.radius;
        this.changeColor();
        this.xVel = -this.xVel + this.xVel / 8;
      }
    }),
    (this.launch = function () {
      // To make the ball launch again when it hits a certain speed
      if (
        this.xVel < 4 &&
        this.xVel > -4 &&
        this.y >= canvas.height - this.radius
      ) {
        // CHANGE TO TELL BALL TO LAUNCH AGAIN EARLIER OR LATER * HIGHER OR LOWER NUMBER *
        this.yVel = -15;
        if (this.xVel < 0) {
          this.xVel = -19;
        } else {
          this.xVel = 19;
        }
      }
    }),
    (this.increaseRadius = function () {
      this.radius += 0.5;
    }),
    (this.decreaseRadius = function () {
      if (this.radius - 0.5 > 0) {
        this.radius -= 0.5;
      }
    }),
    (this.moveUp = function () {
      this.yVel -= 0.5 * this.power;
    }),
    (this.moveDown = function () {
      this.yVel += 0.5 * this.power;
    }),
    (this.moveLeft = function () {
      this.xVel -= 0.3 * this.power;
    }),
    (this.moveRight = function () {
      this.xVel += 0.3 * this.power;
    }),
    (this.activatePower = function () {
      this.power = 2;
    }),
    (this.dashInDirection = function () {
      if (this.xVel < 0) {
        this.x -= 30;
      } else {
        this.x += 30;
      }
      if (this.yVel < 0) {
        this.y -= 30;
      } else {
        this.y += 30;
      }
    });
}

function applyBallPhysics() {
  ball.checkTop();
  ball.checkBottom();
  ball.checkRight();
  ball.checkLeft();
}

function changeBackground() {
  /*if (backgroundIndex === opColors.length) {
		backgroundIndex = 0;
	}
	var color = opColors[backgroundIndex]; // Cannot increment backgroundIndex here since the background is being constantly drawn*/
  var color = opColors[backgroundIndex % opColors.length];
  return color;
}

function draw() {
  ctx.clearRect(0, 0, canvas.width, canvas.height);
  canvas.height = window.innerHeight;
  canvas.width = window.innerWidth * 0.9;
  ball.draw();

  applyBallPhysics();

  // while not playing the game, just launch the ball
  if (!game.isPlaying) {
    ball.launch();
  }

  ball.yVel = ball.yVel + canvas.g / 25; // To have gravity act on the ball gradually

  // If playing the game, draw the target and validate the collision
  if (game.isPlaying) {
    target.draw();

    if (target.isColliding(ball)) {
      game.incrementTargetsHit();
      target.move();
    }
  }

  //*ADDING KEYBOARD FUNCTIONALITY
  if (canvas.keys && canvas.keys["ArrowLeft"]) {
    ball.moveLeft();
  }
  if (canvas.keys && canvas.keys["ArrowUp"]) {
    ball.moveUp();
  }
  if (canvas.keys && canvas.keys["ArrowRight"]) {
    ball.moveRight();
  }
  if (canvas.keys && canvas.keys["ArrowDown"]) {
    ball.moveDown();
  }

  if (canvas.keys && canvas.keys["w"]) {
    ball.increaseRadius();
  }

  if (canvas.keys && canvas.keys["s"]) {
    ball.decreaseRadius();
  }

  if (canvas.keys && canvas.keys["Shift"]) {
    ball.activatePower();
  }
  if (canvas.keys && !canvas.keys["Shift"]) {
    ball.power = 1;
  }

  if (canvas.keys && canvas.keys[" "]) {
    ball.dashInDirection();
  }

  if (canvas.keys && canvas.keys["click"] && canvas.keys["click"]["click"]) {
    // var pos = canvas.keys["click"]["position"];
    // canvas.keys["MouseClick"]["click"] = false;
    // ball.x = pos.x;
    // ball.y = pos.y;
  }

  ball.x += ball.xVel;
  ball.y += ball.yVel;

  window.requestAnimationFrame(draw);
}

function incrementScore() {
  game.targets_hit++;

  // now we should only check the highscore after they've hit all the targets
}

function handleGravityOnOff(checkBox) {
  if (checkBox.checked) {
    canvas.g = 0;
  } else {
    canvas.g = 9.81;
  }
}

function modifyBounciness(rangeSlider) {
  ball.bounciness = rangeSlider.value;
}

function handleDrawLineToTarget(checkBox) {
  drawLineToTarget = checkBox.checked;
}

function handlePlayStopGame() {
  game.isPlaying = !game.isPlaying;
  var buttonCalledOn = $("#playButton").get(0);

  if (game.isPlaying) {
    game.timer.startTimer();
    buttonCalledOn.className = "btn btn-danger";
    buttonCalledOn.innerHTML = "Stop Game";
    $("#gameInfoSection").get(0).style.display = "block";
  } else {
    buttonCalledOn.className = "btn btn-primary";
    buttonCalledOn.innerHTML = "Play Game";

    // incomplete game
    game.timer.stopTimer();
  }
}

function init() {
  canvas = document.getElementById("canvas");
  canvas.height = window.innerHeight;
  canvas.width = window.innerWidth;

  ctx = canvas.getContext("2d");

  addEventListeners();

  canvas.g = 9.81;
  // Set up to start a new game
  game = new Game();
  backgroundIndex = 0;

  ball = new Ball();
  target = new Target(canvas.width / 4 + canvas.width / 5, canvas.height / 2);

  highscore = getStoredHighscore();
  if (highscore == null) {
    highscore = "00 : 00 : 00";
  }

  $("#highscore").get(0).innerHTML = highscore;

  // To start animation when window has loaded
  window.requestAnimationFrame(draw);
}

function getStoredHighscore() {
  var cookies = document.cookie.split("; ");
  for (let i = 0; i < cookies.length; i++) {
    var keyPairValue = cookies[i].split("=");
    if (keyPairValue[0] === "highscore") {
      return keyPairValue[1];
    }
  }
}

function addEventListeners() {
  canvas.addEventListener("keydown", function (e) {
    // Build up an array of keys that are pressed
    if (!canvas.keys) {
      canvas.keys = [];
    }

    canvas.keys[e.key] = e.type === "keydown";

    // to prevent scrollbar interference
    e.preventDefault();
  });
  canvas.addEventListener("keyup", function (e) {
    canvas.keys[e.key] = e.type === "keydown";
  });

  // ! Can probably remove this one
  canvas.addEventListener("click", (event) => {
    var pos = getMousePos(canvas, event);
    var x = pos.x;
    var y = pos.y;
    var items = [x, y, true];
    if (!canvas.keys) {
      canvas.keys = [];
    }
    canvas.keys["click"] = { position: pos, click: true };
    // console.log(canvas.keys["click"]);
  });

  $(window).on("beforeunload", function () {
    var highScoreValue = $("#highscore")[0].innerHTML;

    document.cookie = "highscore=" + highScoreValue + "; max-age=" + 604800;
  });
}

function getMousePos(canvas, evt) {
  var rect = canvas.getBoundingClientRect(), // abs. size of element
    scaleX = canvas.width / rect.width, // relationship bitmap vs. element for X
    scaleY = canvas.height / rect.height; // relationship bitmap vs. element for Y

  return {
    x: (evt.clientX - rect.left) * scaleX, // scale mouse coordinates after they have
    y: (evt.clientY - rect.top) * scaleY, // been adjusted to be relative to element
  };
}

document.addEventListener("DOMContentLoaded", init);

// Possible game: Same random targets, but without touching the ground

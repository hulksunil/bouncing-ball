/**
 * Creates a timer that will bind itself to a label. When started, it'll display the time passed in
 * the label
 */
class Timer {
  constructor(label) {
    // The label will be of format 00:00:00
    this.label = label;
    this.secs = 0;
    this.min = 0;
    this.hour = 0;
    this.chrono = null;
  }
  startTimer() {
    var self = this;
    this.chrono = setInterval(function () {
      self.updateTimer();
    }, 1000);
  }
  updateTimer() {
    this.secs++;
    if (this.secs == 60) {
      this.secs = 0;
      this.min++;
    }
    if (this.min == 60) {
      this.min = 0;
      this.hour++;
    }
    this._updateLabel();
  }
  _updateLabel() {
    let hourStr = this.hour;
    if (this.hour < 10) {
      hourStr = `0${this.hour}`;
    }
    let minStr = this.min;
    if (this.min < 10) {
      minStr = `0${this.min}`;
    }
    let secsStr = this.secs;
    if (this.secs < 10) {
      secsStr = `0${this.secs}`;
    }
    this.label.innerHTML = `${hourStr} : ${minStr} : ${secsStr}`;
  }
  pauseTimer() {
    clearInterval(this.chrono);
  }
  stopTimer() {
    clearInterval(this.chrono);
    this.label.innerHTML = `00 : 00 : 00`;
    this.secs = 0;
    this.min = 0;
    this.hour = 0;
  }
  /**
   * Preferred to call this just before calling stopTimer
   */
  getTime() {
    var hour = this.hour;
    var min = this.min;
    var secs = this.secs;
    return { hour, min, secs };
  }
}

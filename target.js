class Target {
  constructor(x, y) {
    this.x = x;
    this.y = y;
    this.radius = 50;
  }
  draw() {
    var distanceToChangeBy = this.radius / 6;
    var numRings = 6;
    for (let i = 0; i < numRings; i++) {
      ctx.beginPath();
      ctx.arc(
        this.x,
        this.y,
        this.radius - distanceToChangeBy * i,
        0,
        Math.PI * 2,
        false
      );
      ctx.closePath();
      if (i % 2 == 0) {
        ctx.fillStyle = "white";
      } else {
        ctx.fillStyle = "red";
      }
      ctx.fill();
    }
  }
  isColliding(someball) {
    // Might need to modify because the ball could be moving fast and might jump right over the target

    var dx = someball.x - this.x;
    var dy = someball.y - this.y;

    if (drawLineToTarget) {
      ctx.moveTo(someball.x, someball.y);
      ctx.lineTo(this.x, this.y);
      ctx.stroke();
    }

    var distance = Math.sqrt(dx * dx + dy * dy);
    return distance < someball.radius + this.radius;
  }
  move() {
    // The amount of space that the new position must not be within
    // so the new target isn't close to the old one
    var rangeToCheck = 10;

    var min = 100;
    var newX = Math.floor(Math.random() * (canvas.width - 2 * min) + min);
    var newY = Math.floor(Math.random() * (canvas.height - 2 * min) + min);

    // ! Very interesting to surround each while statement with a not
    while (newX < this.x + rangeToCheck && newX > this.x - rangeToCheck) {
      newX = Math.floor(Math.random() * (canvas.width - 2 * min) + min);
    }

    while (newY < this.y + rangeToCheck && newY > this.y - rangeToCheck) {
      newY = Math.floor(Math.random() * (canvas.height - 2 * min) + min);
    }

    this.x = newX;
    this.y = newY;

    // change the size cuz why not
    this.radius = Math.floor(Math.random() * (70 - 20) + 20);
  }
}
